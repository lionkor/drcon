import std.stdio;
import std.socket;
import std.conv;
import std.bitmanip;
import core.runtime;
import std.algorithm.iteration;
import std.string;
import std.ascii;

enum RconPacketType : int32_t
{
    RESPONSE_VALUE = 0,
    EXECCOMMAND_OR_AUTH_RESPONSE = 2,
    AUTH = 3
}

struct RconPacket
{
    int32_t id;
    RconPacketType type;
    string body;
    static RconPacket fromString(RconPacketType type, int32_t id, string str) @safe
    {
        // size of type + size of id + size of end (must be 1)
        return RconPacket(id, type, str);
    }
}

TcpSocket tryConnect(string host, string port) @safe
{
    Address[] addrs = getAddress(host, port);

    TcpSocket socket;
    string lastErr = "";

    Address lastAddr = null;

    foreach (Address addr; addrs)
    {
        try
        {
            if (lastAddr is null || addr.toString() != lastAddr.toString())
            {
                if (!(lastAddr is null))
                {
                    writefln("Error: %s", lastErr);
                }
                lastAddr = addr;
                writef("Trying '%s'... ", addr.toString());
            }
            socket = new TcpSocket(addr.addressFamily);
            socket.connect(addr);
            break;
        }
        catch (SocketException e)
        {
            lastErr = e.msg;
        }
    }

    if (addrs.length == 0 || lastErr.length != 0)
    {
        writefln("Error: %s", lastErr);
        writefln("Failed to connect to '%s' port %s.", host, port);
        throw new Error("Failed to connect");
    }
    else
    {
        writeln("Connected!");
    }
    return socket;
}

class UnexpectedCloseException : Exception
{
    this(string msg = "Connection closed", string file = __FILE__, size_t line = __LINE__) @trusted
    {
        super(msg, file, line);
    }
}

int32_t readI32(ref TcpSocket socket) @safe
{
    ubyte[4] buf32;
    auto n = socket.receive(buf32);
    if (n < 4)
    {
        throw new UnexpectedCloseException("readI32");
    }
    auto range = buf32[];
    auto res = range.read!(int32_t, Endian.littleEndian)();
    return res;
}

string readString(ref TcpSocket socket, size_t n)
{
    ubyte[] buffer;
    buffer.length = n;
    auto read = socket.receive(buffer);
    if (read < n)
    {
        throw new UnexpectedCloseException("readString");
    }
    return cast(string)(buffer);
}

void sendRconPacket(ref TcpSocket socket, RconPacket packet)
{
    ubyte[] buffer;
    auto length = packet.id.sizeof
        + packet.type.sizeof
        + packet.body.length
        + 2;
    buffer.length = length + 4;
    size_t index = 0;
    std.bitmanip.write!(int32_t, Endian.littleEndian)(buffer, cast(int32_t)(length), &index);
    std.bitmanip.write!(int32_t, Endian.littleEndian)(buffer, packet.id, &index);
    std.bitmanip.write!(int32_t, Endian.littleEndian)(buffer, packet.type.to!int32_t(), &index);
    buffer[index .. index + packet.body.length] = cast(ubyte[])(packet.body);
    index += packet.body.length;
    buffer[index .. index + 2] = [0, 0];
    if (socket.send(buffer) <= 0)
    {
        throw new UnexpectedCloseException();
    }
}

enum RconState
{
    NONE,
    WAIT_FOR_RESPONSE,
    IDLE,
    EXITING,
}

int main()
{
    auto args = Runtime.args();
    if (args.length != 3)
    {
        writeln("Invalid arguments, provide '<host> <port>'");
        return -1;
    }

    auto socket = tryConnect(args[1], args[2]);

    auto state = RconState.NONE;
    int32_t globalId = 1;

    int32_t authId;

    while (true)
    {
        try
        {
            switch (state)
            {
            case RconState.NONE:
                // must auth
                char[] pwBuf;
                write("Password: ");
                readln(pwBuf);
                writeln("[Authenticating...]");
                authId = globalId++;
                pwBuf.length -= 1; // remove newline
                auto packet = RconPacket.fromString(RconPacketType.AUTH, authId, pwBuf.to!string());
                socket.sendRconPacket(packet);
                state = RconState.WAIT_FOR_RESPONSE;
                break;
            case RconState.WAIT_FOR_RESPONSE:
                auto recvPacket = RconPacket();
                auto size = socket.readI32();
                recvPacket.id = socket.readI32();
                recvPacket.type = socket.readI32().to!RconPacketType();
                auto toRead = size - (2 * 4) - 2;
                recvPacket.body = socket.readString(toRead);
                // expect null terminator
                ubyte[2] nullByte;
                if (socket.receive(nullByte) <= 0)
                {
                    throw new UnexpectedCloseException("null byte");
                }
                if (!(nullByte[0] == 0 && nullByte[1] == 0))
                {
                    throw new Exception(
                        "Expected null byte as end sentinel of RCON packet, got '" ~ nullByte.to!string() ~ "' instead.");
                }
                switch (recvPacket.type)
                {
                case RconPacketType.RESPONSE_VALUE:
                    if (recvPacket.body.length == 0)
                    {
                        if (recvPacket.id == authId)
                        {
                            // expected for auth response (empty followed by auth response)
                            // ignoring
                        }
                        else
                        {
                            writeln("[Empty packet received.]");
                        }
                    }
                    else
                    {
                        writeln(recvPacket.body);
                    }
                    state = RconState.IDLE;
                    break;
                case RconPacketType.EXECCOMMAND_OR_AUTH_RESPONSE:
                    if (recvPacket.id == cast(int32_t)(-1))
                    {
                        writeln("[Authentication failed.]");
                        state = RconState.EXITING;
                    }
                    else if (recvPacket.id == authId)
                    {
                        writeln("[Authentication successful.]");
                    }
                    else
                    {
                        writeln(recvPacket.body);
                    }
                    state = RconState.IDLE;
                    break;
                default:
                    assert(0);
                }
                break;
            case RconState.IDLE:
                char[] cmd;
                write("> ");
                readln(cmd);
                cmd.length -= 1; // strip newline
                auto packet = RconPacket.fromString(RconPacketType.EXECCOMMAND_OR_AUTH_RESPONSE, globalId++,
                    cmd.to!string());
                socket.sendRconPacket(packet);
                state = RconState.WAIT_FOR_RESPONSE;
                break;
            case RconState.EXITING:
                return 0;
            default:
                assert(0);
            }

        }
        catch (UnexpectedCloseException e)
        {
            writefln("Connection closed in %s:%d: %s", e.file, e.line, e.msg);
            break;
        }
    }
    return 0;
}
